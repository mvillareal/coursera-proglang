(* Coursera Programming Languages, Homework 3 *)

exception NoAnswer

datatype pattern = Wildcard
         | Variable of string
         | UnitP
         | ConstP of int
         | TupleP of pattern list
         | ConstructorP of string * pattern

datatype valu = Const of int
          | Unit
          | Tuple of valu list
          | Constructor of string * valu

fun g f1 f2 p =
    let 
    val r = g f1 f2 
    in
    case p of
        Wildcard          => f1 ()
      | Variable x        => f2 x
      | TupleP ps         => List.foldl (fn (p,i) => (r p) + i) 0 ps
      | ConstructorP(_,p) => r p
      | _                 => 0
    end

val only_capitals =
    List.filter (fn s => Char.isUpper(String.sub(s, 0)))

val longest_string1 =
    List.foldl (fn (x,y) => if String.size x > String.size y then x else y) ""

val longest_string2 =
    List.foldl (fn (x,y) => if String.size x >= String.size y then x else y) ""

fun longest_string_helper f = 
    List.foldl (fn (x,y) => if f(String.size x, String.size y) then x else y) ""

val longest_string3 =
    longest_string_helper (fn (x,y) => x > y)

val longest_string4 =
    longest_string_helper (fn (x,y) => x >= y)

val longest_capitalized = 
    longest_string4 o only_capitals

val rev_string = 
    String.implode o List.rev o String.explode

fun first_answer f [] = raise NoAnswer 
  | first_answer f (x::xs) = 
    case f x of 
        NONE => first_answer f xs
      | SOME y => y

fun all_answers f xs =  
    let
        fun get_answers([], acc) = SOME acc
          | get_answers(x::xs', acc) = 
            case f x of
                NONE => NONE
              | SOME y => get_answers(xs', acc@y)
    in
        get_answers(xs, [])
    end

fun count_wildcards p =
    g (fn x=>1) (fn x=>0) p

fun count_wild_and_variable_lengths p =
    g (fn x=>1) (fn x=>String.size x) p
    
fun count_some_var(s, p) =
    g (fn x=>0) (fn x=>if s=x then 1 else 0) p

fun check_pat(p) =
    let
        fun get_vars p = 
            case p of
                Variable x => [x]
              | TupleP ps => (List.foldl (fn (p,acc) => acc@get_vars p) [] ps)
              | _ => []
        fun has_dupes([]) = false
          | has_dupes(x::xs') =
            if List.exists (fn s=>x=s) xs' 
            then true 
            else has_dupes xs'
    in
        (not o has_dupes o get_vars) p
    end

fun match(valu, p) = 
    case (p, valu) of
        (Wildcard, _)       => SOME []
      | (Variable x, _)     => SOME [(x, valu)]
      | (UnitP, Unit)       => SOME []
      | (ConstP x, Const y) => 
            if x=y 
            then SOME [] 
            else NONE
      | (TupleP ps, Tuple vs) => 
            (all_answers (fn x => match x) (ListPair.zipEq(vs, ps)) handle UnequalLengths => NONE)
      | (ConstructorP(s1,p), Constructor(s2,v)) => if s1=s2 then match(v, p) else NONE
      | _                   => NONE

fun first_match v ps =
    SOME (first_answer (fn x => match(v,x)) ps) handle NoAnswer => NONE;

(**** for the challenge problem only ****)

datatype typ = Anything
         | UnitT
         | IntT
         | TupleT of typ list
         | Datatype of string

fun typecheck(Anything, _) = true
  | typecheck(t, Wildcard) = true
  | typecheck(t, Variable v) = true
  | typecheck(UnitT, UnitP) = true
  | typecheck(IntT, ConstP i) = true
  | typecheck(Datatype s1, ConstructorP(s2, v)) = s1 = s2
  | typecheck(TupleT ts, TupleP ps) = not (List.exists (fn x => not (typecheck x)) (ListPair.zipEq(ts, ps) handle UnequalLengths => [(UnitT, ConstP 2)]))
  | typecheck(_,_) = false

fun typecheck_patterns(ts: (string * string * typ) list, ps) =
    let
        fun typecheck_all(t, []) = true
          | typecheck_all(t, p::ps) =
                if typecheck(t, p) then typecheck_all(t, ps) else false
        fun check_pattern((constructor_name: string, datatype_name: string, t:typ), acc) =
            if typecheck_all(t, ps) then acc@[t] else acc
    in
        case List.foldl (fn (x,acc) =>check_pattern(x,acc)) [] ts of
            [] => NONE
          | x::xs => SOME x (* to do: find the most lenient*)
    end
