#lang racket
(provide (all-defined-out)) 

(define (sequence low high stride)
  (if (> low high)
      null
      (cons low (sequence (+ low stride) high stride))))                

(define (string-append-map xs suffix)
  (map (lambda (s) (string-append s suffix)) xs))

(define (list-nth-mod xs n)
  (cond [(< n 0) (error "list-nth-mod: negative number")] 
        [(null? xs) (error "list-nth-mod: empty list")]
        [#t (car (list-tail xs (remainder n (length xs))))]))

(define (stream-for-n-steps s n)
  (let ([res (s)])
    (cond [(null? res) null]
          [(<= n 0) null]
          [#t (cons (car res) (stream-for-n-steps (cdr res) (- n 1)))])))

(define funny-number-stream
  (letrec ([funnify (lambda(n) 
                      (cons 
                       (if (= (remainder n 5) 0)
                           (* n -1)
                           n)
                       (lambda () (funnify (+ n 1)))))])
    (lambda () (funnify 1))))

(define dan-then-dog
  (letrec ([dan (lambda () (cons "dan.jpg" dog))]
           [dog (lambda () (cons "dog.jpg" dan))])
    dan))

(define (stream-add-zero s)
  (let ([res (s)])
    (lambda() 
      (cons (cons 0 (car res)) (stream-add-zero (cdr res))))))

(define (cycle-lists xs ys)
  (letrec ([make-stream (lambda (n) 
                          (cons 
                           (cons (list-nth-mod xs n) (list-nth-mod ys n)) 
                           (lambda () (make-stream (+ n 1)))))])
    (lambda () (make-stream 0))))

(define (vector-assoc v vec)
  (letrec ([find-vector (lambda (ndx)
                          (if (>= ndx (vector-length vec))
                              #f
                              (let ([elem (vector-ref vec ndx)])
                                (if (and (pair? elem) (equal? (car elem) v))
                                    elem
                                    (find-vector (+ ndx 1))))))])
    (find-vector 0)))

(define (cached-assoc xs n)
  (let ([memo (make-vector n #f)]
        [ndx 0])
    (lambda (v) 
      (let ([ans (vector-assoc v memo)])
        (if ans
            ans
            (let ([new-ans (assoc v xs)])
              (begin ;(print (string-append (~a new-ans) ": " (~a memo) ":" (~a ndx)))
                (vector-set! memo ndx new-ans)
                (if (< ndx (- n 1))
                    (set! ndx (+ ndx 1))
                    (set! ndx 0))
                new-ans)))))))

(define-syntax while-less
  (syntax-rules (do)
    [(while-less e1 do e2)
     (letrec ([val1 e1]
              [loop (lambda () 
                      (if (< e2 val1)
                          (loop)
                          #t))])
       (loop))]))

