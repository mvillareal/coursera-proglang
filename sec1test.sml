use "sec1.sml";

val d1 = (1999,12,31)
val d2 = (2000,1,1)
val d3 = (1900,12,2)
val d4 = (1900,12,1)

val s1 = "dog"
val s2 = "cat"
val s3 = "apple"

val t1_1 = is_older(d1, d2)
val t1_2 = not(is_older(d3, d4))
val t1_3 = not(is_older((1,1,1),(1,1,1)))
val t1_4 = is_older((1,1,1), (1,1,2))
val t1_5 = is_older((1,1,1), (1,2,1))
val t1_6 = not(is_older((1,1,2), (1,1,1)))
val t1_7 = not(is_older((1,2,1), (1,1,1)))

val t2_1 = number_in_month([d1,d2,d3,d4], 12) = 3
val t2_2 = number_in_month([], 1) = 0
val t2_3 = number_in_month([d1, d2, d2, d2, d2], 12) = 1
val t2_4 = number_in_month([d2, d1, d2, d1], 12) = 2

val t3_1 = number_in_months([d1,d2,d3,d4], [6,12,2,5]) = 3
val t3_2 = number_in_months([d1,d2,d3,d4], [6,2,5]) = 0
val t3_3 = number_in_months([d1,d2,d3,d4], [1]) = 1
val t3_4 = number_in_months([d1,d2,d3,d4], [6,2,5,1,12]) = 4

val t4_1 = dates_in_month([d1,d2,d3,d4], 12) = [d1,d3,d4]
val t4_2 = dates_in_month([d1,d2,d3,d4], 1) = [d2]
val t4_3 = dates_in_month([d1,d2,d3,d4], 2) = []

val t5_1 = dates_in_months([d1,d2,d3,d4], [12]) = [d1,d3,d4]
val t5_2 = dates_in_months([d1,d2,d3,d4], [12,2,1]) = [d1,d3,d4,d2]
val t5_3 = dates_in_months([d1,d2,d3,d4], [3]) = []

val t6_1 = get_nth([s1, s2, s3], 1) = "dog"
val t6_2 = get_nth([s1, s2, s3], 2) = "cat"
val t6_3 = get_nth([s1, s2, s3], 3) = "apple"

val t7_1 = date_to_string(d1) = "December 31, 1999"
val t7_2 = date_to_string(d2) = "January 1, 2000"

val t8_1 = number_before_reaching_sum(10, [1,2,3,4,5]) = 5
val t8_2 = number_before_reaching_sum(2, [1,2,3,4,5]) = 2
val t8_3 = number_before_reaching_sum(15, [1,2,3,4,5]) = 5
val t8_4 = number_before_reaching_sum(1, [1,2]) = 2

val t9_1 = what_month(1) = 1
val t9_2 = what_month(31) = 1
val t9_3 = what_month(32) = 2
val t9_4 = what_month(365) = 12

val t10_1 = month_range(1,2) = [1,1]
val t10_2 = month_range(31,33) = [1, 2, 2]
val t10_3 = month_range(2,1) = []

val t11_1 = oldest([d1,d2]) = SOME
 d1
val t11_2 = oldest([d1,d2,d3,d4]) = SOME d4
val t11_3 = oldest([]) = NONE

val rm_1 = remove_dupes([1,2,3]) = [1,2,3]
val rm_2 = remove_dupes([2,3,2]) = [3,2]
val rm_3 = remove_dupes([1,2,3,3,3,1,2]) = [3,1,2]



