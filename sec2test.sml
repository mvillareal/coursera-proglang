use "sec2.sml";

val l1 = ["dog", "cat", "apple"];
val t1a1 = all_except_option("dog", l1) = SOME ["cat", "apple"];
val t1a2 = all_except_option("fish", l1) = NONE;
val t1a3 = all_except_option("apple", l1) = SOME ["dog", "cat"];
val t1a4 = all_except_option("fish", ["fish"]) = SOME [];

val subs1 = [["Fred","Fredrick"],["Elizabeth","Betty"],["Freddie","Fred","F"]]
val subs2 = [["Fred","Fredrick"],["Jeff","Jeffrey"],["Geoff","Jeff","Jeffrey"]]
val t1b1 = get_substitutions1(subs1, "Fred") = ["Fredrick","Freddie","F"]
val t1b2 = get_substitutions1(subs1, "Betty") = ["Elizabeth"]
val t1b3 = get_substitutions1(subs1, "A") = []
val t1b4 = get_substitutions1(subs2, "Jeff") = ["Jeffrey","Geoff","Jeffrey"]
val t1b5 = get_substitutions1([], "") = []

val t1c1 = get_substitutions2(subs1, "Fred") = ["Fredrick","Freddie","F"]
val t1c2 = get_substitutions2(subs1, "Betty") = ["Elizabeth"]
val t1c3 = get_substitutions2(subs1, "A") = []
val t1c4 = get_substitutions2(subs2, "Jeff") = ["Jeffrey","Geoff","Jeffrey"]
val t1c5 = get_substitutions2([], "") = []

val t1d1 = similar_names(subs1, {first="Fred", middle="W", last="Smith"}) = 
	[{first="Fred", last="Smith", middle="W"},
	{first="Fredrick", last="Smith", middle="W"},
	{first="Freddie", last="Smith", middle="W"},
	{first="F", last="Smith", middle="W"}]
val t1d2 = similar_names(subs1, {first="A", middle="X", last="Smith"}) = 
	[{first="A", last="Smith", middle="X"}]
val t1d3 = similar_names(subs1, {first="Betty", middle="W", last="Zoo"}) = 
	[{first="Betty", last="Zoo", middle="W"}, {first="Elizabeth", last="Zoo", middle="W"}]
val t1d4 = similar_names([], {first="Betty", middle="W", last="Smith"}) = [{first="Betty", middle="W", last="Smith"}]

val c1 = (Clubs, Num 5)
val c2 = (Hearts, Queen)
val c3 = (Diamonds, Num 2)
val c4 = (Spades, Ace)

val t2a1 = card_color(c1) = Black
val t2a2 = card_color(c2) = Red
val t2a3 = card_color(c3) = Red
val t2a4 = card_color(c4) = Black

val t2b1 = card_value(c1) = 5
val t2b2 = card_value(c2) = 10
val t2b3 = card_value(c3) = 2
val t2b4 = card_value(c4) = 11

val cs1 = [c1, c2, c3, c4];
val cs2 = [c1, c1, c3, c4];
val t2c1 = remove_card(cs1, c1, IllegalMove) = [c2, c3, c4];
val t2c2 = (remove_card([], c2, IllegalMove); false) handle IllegalMove => true;
val t2c3 = (remove_card(cs1, (Clubs, Num 1), IllegalMove); false) handle IllegalMove => true;
val t2c4 = remove_card(cs1, c4, IllegalMove) = [c1, c2, c3];
val t2c5 = remove_card(cs2, c1, IllegalMove) = [c1, c3, c4];

val t2d1 = all_same_color(cs1) = false
val t2d2 = all_same_color([c1, c4]) = true
val t2d3 = all_same_color([]) = true
val t2d4 = all_same_color([c1, c1]) = true

val t2e1 = sum_cards(cs1) = 28;
val t2e2 = sum_cards(cs2) = 23;
val t2e3 = sum_cards([]) = 0;
val t2e4 = sum_cards([c3]) = 2;
val t2e5 = sum_cards([c1, c2]) = 15;

val t2f1 = score(cs1, 10) = 54;
val t2f2 = score(cs1, 29) = 1;
val t2f3 = score([], 29) = 14;
val t2f4 = score([c1,c4], 29) = 6;
val t2f5 = score([c1,c4], 2) = 21;
val t2f6 = score([], 0) = 0
val t2f7 = score([c2, c3], 10) = 3;
val t2f8 = score([c2], 10) = 0;

val t2g1 = officiate(cs1, [Draw, Draw, Draw, Draw], 10) = 15;
val t2g2 = officiate(cs1, [Draw, Draw, Draw, Discard c1], 10) = 15;
val t2g3 = officiate(cs1, [Draw, Draw, Draw, Draw, Draw], 29) = 1;
val t2g4 = (officiate(cs1, [Draw, Draw, Draw, Discard c4], 30); false) handle IllegalMove => true
val t2g5 = (officiate(cs1, [Discard c1], 10); false) handle IllegalMove => true
val t2g6 = officiate(cs1, [], 10) = 5;
val t2g7 = officiate([], [Draw], 0) = 0;
val t2g8 = officiate(cs1, [Draw], 0) = 7;
val t2g9 = (officiate([], [Discard c1], 10); false) handle IllegalMove => true
val t2g10 = officiate(cs1, [Draw], 6) = 0;
val t2g11 = officiate(cs1, [Draw, Draw], 0) = 7;
val t2g12 = officiate(cs1, [Draw, Draw, Draw, Discard c2], 30) = 23;

fun provided_test1 () = 
  let val cards = [(Clubs,Jack),(Spades,Num(8))]
    val moves = [Draw,Discard(Hearts,Jack)]
  in
    officiate(cards,moves,42)
  end

fun provided_test2 () = 
  let val cards = [(Clubs,Ace),(Spades,Ace),(Clubs,Ace),(Spades,Ace)]
    val moves = [Draw,Draw,Draw,Draw,Draw]
  in
    officiate(cards,moves,42)
  end

val pt1 = (provided_test1();false) handle IllegalMove => true;
val pt2 = (provided_test2() = 3);

val t3a1 = score_challenge(cs1, 10) = 24;
val t3a2 = score_challenge(cs1, 29) = 1;
val t3a3 = score_challenge([], 29) = 14;
val t3a4 = score_challenge([c1,c4], 29) = 6;
val t3a5 = score_challenge([c1,c4], 6) = 0;
val t3a6 = score_challenge([], 0) = 0
val t3a7 = score_challenge([c2, c3], 10) = 3;
val t3a8 = score_challenge([c2], 10) = 0;
val t3a9 = score_challenge([c1,c4], 8) = 1;
val t3a10 = score_challenge([c2,c4], 20) = 3;

val t3b1 = find_card_value(cs1, 10) = SOME c2;
val t3b2 = find_card_value(cs1, 3) = NONE;
val t3b3 = abs(201) = 201;
val t3b4 = abs(~1) = 1;
val t3b5 = find_card_value(cs1, abs(sum_cards([]) - 2)) = SOME c3

val t3c1 = careful_player(cs1, 15) = [Draw]
val t3c2 = careful_player([(Clubs, Num 4), (Spades, Num 6), (Hearts, Ace), (Diamonds, Num 2)], 15) = [Draw,Draw,Discard (Spades,Num 6),Draw]
val t3c3 = careful_player( [ (Clubs, Ace), (Spades, Jack), (Hearts, Num 4) ], 22) = [Draw,Draw]
val t3c4 = careful_player( [ (Clubs, Ace), (Spades, Jack), (Hearts, Num 4) ], 21) = [Draw]
val t3c5 = careful_player(cs1, 16) = [Draw,Draw]

