use "sec3.sml";

val ss1 = ["a", "apple", "Dog", "cat", "APPLE"];
val ss2 = ["APPLE", "Banana", "cabbage", "1234567", "cat"];
val ss3 = ["a", "apple", "Dog", "cat", "APPLE", "carrot", "Apple"];
val ss4 = ["a", "apple", "dog", "cat", "aPPLE", "carrot", "apple"];

val t1a = only_capitals [] = [];
val t1b = only_capitals ss1 = ["Dog", "APPLE"];
val t1c = only_capitals ss2 = ["APPLE", "Banana"];

val t2a = longest_string1 ss1 = "apple";
val t2b = longest_string1 ss2 = "cabbage";
val t2c = longest_string1 [] = "";

val t3a = longest_string2 ss1 = "APPLE";
val t3b = longest_string2 ss2 = "1234567";

val t4a = longest_string3 ss1 = "apple";
val t4b = longest_string3 ss2 = "cabbage";
val t4c = longest_string3 [] = "";

val t5a = longest_string4 ss1 = "APPLE";
val t5b = longest_string4 ss2 = "1234567";

val t7a = longest_capitalized ss1 = "APPLE";
val t7b = longest_capitalized ss2 = "Banana";
val t7c = longest_capitalized (ss1@ss2) = "Banana";
val t7d = longest_capitalized [] = "";
val t7e = longest_capitalized ss3 = "Apple";
val t7f = longest_capitalized (ss1@ss2@ss3) = "Banana";
val t7g = longest_capitalized ss4 = "";

val t8a = rev_string "Apple" = "elppA";
val t8b = rev_string "" = "";
val t8c = rev_string "b" = "b";

fun does_word_have_letter letter = 
    (List.find (fn x => x = letter)) o String.explode

val test = Char.contains "apple" #"a";

fun does_word_have_letter w c = if Char.contains w c then SOME w else NONE;
fun does_word_have_letter_c w = does_word_have_letter w #"c";
fun does_word_have_letter_z w = does_word_have_letter w #"z";

fun len_of_word_with_letter w c = if Char.contains w c then SOME (String.size w) else NONE;
fun len_of_word_word_with_letter_c w = len_of_word_with_letter w #"c";

val t9a = first_answer (fn x => NONE) ss1 handle NoAnswer => true;
val t9b = first_answer does_word_have_letter_c ss1 = "cat";
val t9c = (first_answer does_word_have_letter_z ss1;false) handle NoAnswer => true;
val t9d = (first_answer does_word_have_letter_c [] = "cat") handle NoAnswer => true;;
val t9e = first_answer (fn x => does_word_have_letter x #"3") ss2 = "1234567";
val t9f = first_answer len_of_word_word_with_letter_c ss1 = 3;

fun letters_that_are_not(c, [], acc) = if null acc then NONE else SOME acc
  | letters_that_are_not(c, x::xs', acc) =
    if Char.compare(x,c) = EQUAL
    then letters_that_are_not(c, xs', acc)
    else letters_that_are_not(c, xs', acc@[x])

fun letters_that_are_not_c w = letters_that_are_not(#"c", String.explode w, []);

val t10a = letters_that_are_not_c("ace") = SOME[#"a", #"e"];
val t10b = letters_that_are_not_c("dog") = SOME[#"d", #"o", #"g"];
val t10c = all_answers (fn x => NONE) [] = SOME [];
val t10d = all_answers letters_that_are_not_c [] = SOME [];
val t10e = all_answers letters_that_are_not_c ["c", "cc"] = NONE;
val t10f = all_answers letters_that_are_not_c ["cat", "carrot"] = SOME[#"a", #"t", #"a", #"r", #"r", #"o", #"t"];

val t11a = count_wildcards(Wildcard) = 1;
val t11b = count_wildcards(TupleP([Wildcard, ConstructorP("a", Wildcard), Variable("z"), Wildcard])) = 3;
val t11c = count_wildcards(Variable("z")) = 0;
val t11d = count_wildcards(TupleP([])) = 0;

val t12a = count_wild_and_variable_lengths(Wildcard) = 1;
val t12b = count_wild_and_variable_lengths(TupleP([Wildcard, ConstructorP("a", Wildcard), Variable("z"), Wildcard])) = 4;
val t12c = count_wild_and_variable_lengths(TupleP([Wildcard, ConstructorP("abc", Wildcard), Variable("zawd"), Wildcard])) = 7;
val t12d = count_wild_and_variable_lengths(Variable("apple")) = 5;

val t13a = count_some_var("apple", Wildcard) = 0;
val t13b = count_some_var("cat", TupleP([Wildcard, ConstructorP("a", Wildcard), Variable("cat"), Wildcard])) = 1;
val t13c = count_some_var("z", TupleP([Wildcard, ConstructorP("abc", Wildcard), Variable("zawd"), Wildcard])) = 0;
val t13d = count_some_var("apple", Variable("apple")) = 1;
val t13e = count_some_var("z", TupleP([Wildcard, Variable("z"), ConstructorP("a", Wildcard), Variable("z"), Wildcard])) = 2;
val t13f = count_some_var("z", TupleP([])) = 0;

val t14a = check_pat(Wildcard) = true;
val t14b = check_pat(TupleP([Wildcard, ConstructorP("a", Wildcard), Variable("cat"), Wildcard])) = true;
val t14c = check_pat(TupleP([Wildcard, ConstructorP("cat", Wildcard), Variable("cat"), Wildcard])) = true;
val t14d = check_pat(TupleP([Wildcard, Variable("cat"), ConstructorP("cat", Wildcard), Variable("cat"), Wildcard])) = false;
val t14e = check_pat(TupleP([Variable("dog"), Wildcard, Variable("cat"), ConstructorP("cat", Wildcard), Variable("horse"), Wildcard])) = true;
val t14f = check_pat(TupleP[]) = true;

val valu1 = Tuple[Const 21, Unit];
val valu2 = Tuple[Constructor("sum", Const 27)];

val t15a = match(Const 17, Wildcard) = SOME [];
val t15b = match(valu1, Wildcard) = SOME [];
val t15c = match(valu1, TupleP[Wildcard]) = NONE;
val t15d = match(valu1, TupleP[ConstP 17, UnitP]) = NONE;
val t15e = match(valu1, TupleP[ConstP 21, UnitP]) = SOME [];
val t15f = match(valu1, TupleP[ConstP 21, Wildcard]) = SOME [];
val t15g = match(valu1, TupleP[ConstP 21, Variable("sum")]) = SOME [("sum", Unit)];
val t15h = match(Const 21, ConstP 21) = SOME [];
val t15i = match(Const 22, UnitP) = NONE;
val t15j = match(Unit, UnitP) = SOME [];
val t15k = match(Unit, ConstP 2) = NONE;
val t15l = match(Constructor("void", Constructor("nested", Const 2)), ConstP 2) = NONE;
val t15m = match(Constructor("void", Unit), ConstructorP("notvoid", UnitP)) = NONE;
val t15n = match(Constructor("void", Unit), ConstructorP("void", UnitP)) = SOME [];
val t15o = match(Constructor("int", Const 6), Variable("ave")) = SOME [("ave", Constructor("int", Const 6))];
val t15p = match(Const 1, Variable("sum")) = SOME [("sum", Const 1)];
val t15q = match(valu2, TupleP[ConstructorP("sum", ConstP 12)]) = NONE;
val t15r = match(Tuple[Const 27, Const 22, Unit], TupleP[Variable("var1"), Variable("var2"), Variable("var3")]) = 
    SOME [("var1", Const 27), ("var2", Const 22), ("var3", Unit)];
val t15s = match(Tuple[], TupleP[]) = SOME [];

val t16a = first_match (Const 17) [Wildcard] = SOME [];
val t16b = first_match (Const 17) [ConstP 12, ConstP 31, UnitP] = NONE;
val t16c = first_match (Const 17) [ConstP 12, ConstP 17] = SOME [];
val t16d = first_match (Const 17) [ConstP 12, Variable("a")] = SOME [("a", Const 17)];

val v17a = typecheck(UnitT, Wildcard) = true;
val v17b = typecheck(UnitT, ConstP 12) = false;
val v17c = typecheck(UnitT, Variable "bar")  = true;
val v17d = typecheck(TupleT[UnitT,IntT, Datatype "test"], ConstP 12)  = false;
val v17e = typecheck(TupleT[UnitT,IntT, Datatype "test"], TupleP[Wildcard])  = false; (*REVIEW THIS*)
val v17f = typecheck(TupleT[UnitT,IntT, Datatype "test"], TupleP[Wildcard, Wildcard, Variable "x"])  = true;

val t18a = typecheck_patterns([("foo", "bar", IntT)], [Wildcard]) = SOME IntT;
val t18b = typecheck_patterns([("foo", "bar", IntT)], [TupleP[Variable "foo", Variable "bar"], TupleP[Wildcard, Wildcard] ]) = NONE;
val t18c = typecheck_patterns([("foo", "bar", IntT)], [TupleP[Variable "foo", Variable "bar"], TupleP[Wildcard, Wildcard] ]) = SOME (TupleT[Anything, Anything]);
val t18d = typecheck_patterns([("foo", "bar", IntT)], [TupleP[Wildcard, Wildcard], TupleP[Wildcard, TupleP[Wildcard, Wildcard]] ]) = SOME (TupleT[Anything, TupleT[Anything, Anything]]);
val t18e = typecheck_patterns ( [ ("Abc", "typeA", TupleT [Anything]) ], [ConstructorP ("Abc", TupleP[ConstP 1]) ] ) = SOME (Datatype "TypeA");

