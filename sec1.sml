fun is_older(d1:int*int*int, d2:int*int*int) =
    if (#1 d1 < #1 d2)
    then true
    else if (#1 d1 > #1 d2)
    then false
    else if (#2 d1 < #2 d2)
    then true
    else if (#2 d1 > #2 d2)
    then false
    else (#3 d1 < #3 d2)

fun number_in_month(ds:(int*int*int) list, m:int) =
    if null ds
    then 0
    else if (#2 (hd ds)) = m
    then 1 + number_in_month(tl ds, m)
    else number_in_month(tl ds, m)

fun number_in_months(ds:(int*int*int) list, ms: int list) =
    if null ms
    then 0
    else number_in_month(ds, hd ms) + number_in_months(ds, tl ms)

fun dates_in_month(ds:(int*int*int) list, m:int) =
    if null ds
    then []
    else if (#2 (hd ds)) = m
    then (hd ds)::dates_in_month(tl ds, m)
    else dates_in_month(tl ds, m)

fun dates_in_months(ds:(int*int*int) list, ms:int list) = 
    if null ms
    then []
    else dates_in_month(ds, hd ms) @ dates_in_months(ds, tl ms)

fun get_nth(ss:string list, n:int) =
    if n = 1
    then hd ss
    else get_nth(tl ss, n-1)

fun date_to_string(d:int*int*int) =
    let
	val months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    in
	get_nth(months, (#2 d)) ^ " " ^ Int.toString(#3 d) ^ ", " ^ Int.toString(#1 d)
    end

fun number_before_reaching_sum(sum:int, l:int list) = 
    let
	fun sum_up(cur_sum:int, cur_count:int, l:int list) = 
	    if null l
	    then cur_count - 1
	    else if cur_sum >= sum
	    then cur_count - 1
	    else sum_up(cur_sum + hd l, cur_count + 1, tl l)
    in
	sum_up(hd l, 1, tl l)
    end

val days_in_months = [31,28,31,30,31,30,31,31,30,31,30,31]

fun what_month(day:int) =
    number_before_reaching_sum(day, days_in_months) + 1

fun month_range(day1:int, day2:int) =
    if day1 > day2
    then []
    else what_month(day1)::month_range(day1+1, day2)

fun oldest(dates:(int*int*int) list) =
    if null dates
    then NONE
    else let 
	fun oldest_nonempty(dates:(int*int*int) list) =
	    if null (tl dates)
	    then hd dates
	    else let val oldest_date = oldest_nonempty(tl dates) 
		 in
		     if is_older(hd dates, oldest_date)
		     then hd dates
		     else oldest_date
		 end
    in
	SOME (oldest_nonempty(dates))
    end

fun remove_dupes(l:int list) =
    let
	fun find_in_list(i:int, l: int list) =
	    if null l
	    then false
	    else if i = hd l
	    then true
	    else find_in_list(i, tl l)
    in 
	if null l
	then []
	else if find_in_list(hd l, tl l)
	then remove_dupes(tl l)
	else hd l::remove_dupes(tl l)
    end

fun number_in_months_challenge(ds:(int*int*int) list, ms: int list) =
    number_in_months(ds, remove_dupes(ms))

fun dates_in_months_challenge(ds:(int*int*int) list, ms:int list) = 
    dates_in_months(ds, remove_dupes(ms))

fun get_nth_int(l: int list, n:int) =
    if n = 1
    then hd l
    else get_nth_int(tl l, n-1)

fun is_leap_year(y:int) = 
    y mod 400 = 0 orelse
    y mod 4 = 0 andalso y mod 100 <> 0

fun reasonable_date(d:int*int*int) =
    (#1 d) > 0 andalso 
    (#2 d) > 0 andalso (#2 d) <= 12 andalso
    (#3 d) > 0 andalso 
    (
      if (#2 d) = 2 andalso is_leap_year(#1 d) 
      then (#3 d) <= 29
      else (#3 d) <= get_nth2(days_in_months, (#2 d))
    )
