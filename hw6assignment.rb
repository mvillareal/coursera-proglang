# University of Washington, Programming Languages, Homework 6, hw6runner.rb

class MyTetris < Tetris
	def set_board
		@canvas = TetrisCanvas.new
		@board = MyBoard.new(self)
		@canvas.place(@board.block_size * @board.num_rows + 3,
					  @board.block_size * @board.num_columns + 6, 24, 80)
		@board.draw
	end

	def key_bindings  
		super
		@root.bind('u', proc {@board.rotate_180})
		@root.bind('c', proc {@board.cheat})
	end

end

class MyPiece < Piece
	All_My_Pieces = [rotations([[0, 0], [-1, 0], [0, -1], [-1, -1], [1,0]]), 
		[[[0, 0], [-1, 0], [1, 0], [-2, 0], [2, 0]], # very long (only needs two) 
		[[0, 0], [0, -1], [0, 1], [0, -2], [0, 2]]],
		rotations([[0, 0], [0, -1], [1, 0]])]; # tiny L
	All_My_Pieces.concat(All_Pieces)

	def self.next_piece (board)
		MyPiece.new(All_My_Pieces.sample, board)
	end
end

class MyBoard < Board
	def initialize (game)
		@grid = Array.new(num_rows) {Array.new(num_columns)}
		@current_block = MyPiece.next_piece(self)
		@score = 0
		@game = game
		@delay = 500
		@cheat_next = false
	end

	def next_piece
		if @cheat_next
			@current_block = MyPiece.new([[[0,0]]], self)
			@cheat_next = false
		else
			@current_block = MyPiece.next_piece(self)
		end
		@current_pos = nil
	end

	# gets the information from the current piece about where it is and uses this
	# to store the piece on the board itself.  Then calls remove_filled.
	def store_current
		locations = @current_block.current_rotation
		displacement = @current_block.position
		(0..locations.length).each{|index| 
		  current = locations[index];
		  @grid[current[1]+displacement[1]][current[0]+displacement[0]] = 
		  @current_pos[index] unless current.nil?
		}
		remove_filled
		@delay = [@delay - 2, 80].max
	end

	def rotate_180
		if !game_over? and @game.is_running?
		  @current_block.move(0, 0, 2)
		end
		draw
	end

	def cheat
		if @score >= 100 and !@cheat_next
			@score -= 100
			@cheat_next = true
		end
	end
end
