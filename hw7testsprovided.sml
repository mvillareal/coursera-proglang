(* University of Washington, Programming Languages, Homework 7
   hw7testsprovided.sml *)
(* Will not compile until you implement preprocess and eval_prog *)

(* These tests do NOT cover all the various cases, especially for intersection *)

use "hw7.sml";

fun real_equal(x,y) = Real.compare(x,y) = General.EQUAL;

(* Preprocess tests *)
let
	val Point(a,b) = preprocess_prog(LineSegment(3.2,4.1,3.2,4.1))
	val Point(c,d) = Point(3.2,4.1)
in
	if real_equal(a,c) andalso real_equal(b,d)
	then (print "OK - preprocess converts a LineSegment to a Point successfully\n")
	else (print "X - preprocess does not convert a LineSegment to a Point succesfully\n")
end;

let 
	val LineSegment(a,b,c,d) = preprocess_prog (LineSegment(3.2,4.1,3.1,~4.1))
	val LineSegment(e,f,g,h) = LineSegment(3.1,~4.1,3.2,4.1)
in
	if real_equal(a,e) andalso real_equal(b,f) andalso real_equal(c,g) andalso real_equal(d,h)
	then (print "OK - preprocess flips an improper LineSegment successfully\n")
	else (print "X - preprocess does not flip an improper LineSegment successfully\n")
end;

let 
	val LineSegment(a,b,c,d) = preprocess_prog (LineSegment(3.2,4.1,~3.2,~4.1))
	val LineSegment(e,f,g,h) = LineSegment(~3.2,~4.1,3.2,4.1)
in
	if real_equal(a,e) andalso real_equal(b,f) andalso real_equal(c,g) andalso real_equal(d,h)
	then (print "OK - preprocess flips an improper LineSegment successfully\n")
	else (print "X - preprocess does not flip an improper LineSegment successfully\n")
end;

val l1 = Line(1.0, 2.0)
val l2 = VerticalLine(3.3)
val p1 = Point(2.3,3.4)
val p2 = Point(2.0,4.4)
val ls1 = LineSegment(2.1,3.3,2.2,3.0)
val t1a = case (preprocess_prog p1, p1) of
		(Point pp1, Point pp2) => real_close_point pp1 pp2
		| _ => false;

val t1b = case ((eval_prog (preprocess_prog (Shift(2.0, 3.0, l1)), [])), Line(1.0,3.0)) of
		(Line ll1, Line ll2) => real_close_point ll1 ll2
		| _ => false;

val t1c = case ((eval_prog (preprocess_prog (Shift(2.0, 3.0, NoPoints)), [])), NoPoints) of
		(NoPoints, NoPoints) => true
		| _ => false;

val t1d = case ((eval_prog (preprocess_prog (Shift(2.0, 3.0, l2)), [])), VerticalLine(5.3)) of
		(VerticalLine ll1, VerticalLine ll2) => real_close(ll1, ll2)
		| _ => false;

val t1e = case ((eval_prog (preprocess_prog (Shift(2.0, 3.0, ls1)), [])), LineSegment(4.1, 6.3, 4.2, 6.0)) of
		(LineSegment (a,b,c,d), LineSegment (e,f,g,h)) => real_equal(a,e) andalso real_equal(b,f) andalso real_equal(c,g) andalso real_equal(d,h)
		| _ => false;

(* eval_prog tests with Shift*)
let 
	val Point(a,b) = (eval_prog (preprocess_prog (Shift(3.0, 4.0, Point(4.0,4.0))), []))
	val Point(c,d) = Point(7.0,8.0) 
in
	if real_equal(a,c) andalso real_equal(b,d)
	then (print "OK - eval_prog with empty environment worked\n")
	else (print "X - eval_prog with empty environment is not working properly\n")
end;

(* Using a Var *)
let 
	val Point(a,b) = (eval_prog (Shift(3.0,4.0,Var "a"), [("a",Point(4.0,4.0))]))
	val Point(c,d) = Point(7.0,8.0) 
in
	if real_equal(a,c) andalso real_equal(b,d)
	then (print "OK - eval_prog with 'a' in environment is working properly\n")
	else (print "X - eval_prog with 'a' in environment is not working properly\n")
end;


(* With Variable Shadowing *)
let 
	val Point(a,b) = (eval_prog (Shift(3.0,4.0,Var "a"), [("a",Point(4.0,4.0)),("a",Point(1.0,1.0))]))
	val Point(c,d) = Point(7.0,8.0) 
in
	if real_equal(a,c) andalso real_equal(b,d)
	then (print "OK - eval_prog with shadowing 'a' in environment is working properly\n")
	else (print "X - eval_prog with shadowing 'a' in environment is not working properly\n")
end;
