(* ----- Problem 1 ----- *)
fun same_string(s1 : string, s2 : string) =
    s1 = s2

fun all_except_option(s, ss) = 
    let fun f(xs, acc) = 
        case xs of
            [] => NONE
          | x::xs' => 
            if same_string(s, x) 
            then SOME (acc@xs') 
            else f(xs', acc@[x])
    in
        f(ss, [])
    end

fun get_substitutions1([], _) = []
  | get_substitutions1(x::xs', s) = 
    case all_except_option(s, x) of
        NONE => get_substitutions1(xs', s)
      | SOME y => y @ get_substitutions1(xs', s)

fun get_substitutions2(sub_list, s) =
    let fun f([], acc) = acc
          | f(x::xs', acc) =
            case (all_except_option(s, x), acc) of
                (NONE, _) => f(xs', acc)
              | (SOME ys, []) => f(xs', ys)
              | (SOME ys, _) => f(xs', acc@ys)
    in
        f(sub_list, [])
    end

fun similar_names(sub_list, {first=f, middle=m, last=l}) =
    let fun expand([], acc) = acc
          | expand(x::xs', acc) = expand(xs', acc @ [{first=x, middle=m, last=l}])
    in
        expand(get_substitutions2(sub_list, f), [{first=f, middle=m, last=l}])
    end

(* ----- Problem 2 ----- *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int 
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw 

exception IllegalMove

fun card_color(s, r) = 
    case s of
        Clubs => Black
      | Spades => Black
      | _ => Red

fun card_value(s, r) = 
    case r of
        Num i => i
      | Ace => 11
      | _ => 10

fun remove_card(cards, c, e) =
    let fun f(xs, acc) = 
        case xs of
            [] => raise e
          | x::xs' => 
            if c = x
            then acc@xs'
            else f(xs', acc@[x])
    in
        f(cards, [])
    end

fun all_same_color([]) = true
  | all_same_color(_::[]) = true
  | all_same_color(x::(y::xs')) =
        if card_color(x) = card_color(y) 
        then all_same_color(y::xs')
        else false

fun sum_cards(cards) =
    let fun f([], acc) = acc
          | f(x::xs', acc) = 
                f(xs', acc + card_value(x))
            in
                f(cards, 0)
            end 

fun score(cards, goal) = 
    let
        val sum = sum_cards cards
        val prelim_score = 
            if sum > goal
            then 3 * (sum - goal)
            else goal - sum
    in
        if all_same_color cards 
        then prelim_score div 2
        else prelim_score
    end

fun officiate(cards, moves, goal) = 
    let
        fun move(_, [], held_cards) = score(held_cards,goal)
          | move([], Draw::_, held_cards) = score(held_cards,goal)
          | move([], (Discard dc)::_, held_cards) = raise IllegalMove
          | move(cs, (Discard dc)::ms', held_cards) = move(cs, ms', remove_card(held_cards, dc, IllegalMove))
          | move(c::cs', Draw::ms', held_cards) = 
            let
                val new_held_cards = c::held_cards
            in
                if sum_cards(new_held_cards) > goal
                then score(new_held_cards, goal)
                else move(cs', ms', new_held_cards)
            end
    in
        move(cards, moves, [])
    end

fun sum_cards_with_aces([], sum, _, _) = sum
  | sum_cards_with_aces(c::cs', sum, num_high_aces, goal) = 
        let
            val cv = card_value(c);
            val is_ace = cv = 11
        in
            if sum + cv <= goal
            then sum_cards_with_aces(cs', sum + cv, (if is_ace then 1 else 0) + num_high_aces, goal)
            else 
                if is_ace
                then sum_cards_with_aces(cs', sum + 1, num_high_aces, goal)
                else 
                    if num_high_aces > 0
                    then sum_cards_with_aces(cs', sum + cv - 10, num_high_aces - 1, goal)
                    else sum_cards_with_aces(cs', sum + cv, num_high_aces, goal)
        end

fun score_challenge(cards, goal) = 
    let
        val sum = sum_cards_with_aces(cards, 0, 0, goal);
        val prelim_score = 
            if sum > goal
            then 3 * (sum - goal)
            else goal - sum
    in
        if all_same_color cards 
        then prelim_score div 2
        else prelim_score
    end

fun officiate_challenge(cards, moves, goal) = 
    let
        fun move(_, [], held_cards) = score_challenge(held_cards,goal)
          | move([], Draw::_, held_cards) = score_challenge(held_cards,goal)
          | move([], (Discard dc)::_, held_cards) = raise IllegalMove
          | move(cs, (Discard dc)::ms', held_cards) = move(cs, ms', remove_card(held_cards, dc, IllegalMove))
          | move(c::cs', Draw::ms', held_cards) = 
            let
                val new_held_cards = c::held_cards
            in
                if sum_cards_with_aces(new_held_cards, 0, 0, goal) > goal
                then score_challenge(new_held_cards, goal)
                else move(cs', ms', new_held_cards)
            end
    in
        move(cards, moves, [])
    end

fun find_card_value([], _) = NONE
  | find_card_value(c::cs', v) =
        if card_value(c) = v
        then SOME c
        else find_card_value(cs', v)

fun abs(i) =
    if i > 0
    then i
    else ~i

fun careful_player(cards, goal) =
    let 
        fun move([], moves, _) = moves
          | move(next_card::cs', moves, held_cards) =
            let
                val sum = sum_cards(held_cards)
            in
                if score(held_cards, goal) = 0
                then moves
                else if goal - sum > 10
                then move(cs', moves@[Draw], next_card::held_cards)
                else
                    case find_card_value(held_cards, abs(sum + card_value(next_card) - goal)) of
                        NONE => moves
                      | SOME x => moves@[Discard x,Draw]
            end
    in
        move(cards, [], [])
    end






